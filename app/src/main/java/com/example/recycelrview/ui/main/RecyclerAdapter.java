package com.example.recycelrview.ui.main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recycelrview.R;
import com.example.recycelrview.pojo.Post;

import java.util.ArrayList;
import java.util.List;

// TODO (step 8) Update this Adapter class .

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.PostViewHolder> {
    private List<Post> moviesList = new ArrayList<>();

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PostViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {

        holder.titleTV.setText(moviesList.get(position).getMain().getTempMax()+"");
        holder.userTV.setText(moviesList.get(position).getMain().getTempMin()+"");
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public void setList(List<Post> moviesList) {
        this.moviesList = moviesList;
        notifyDataSetChanged();
    }

    public class PostViewHolder extends RecyclerView.ViewHolder {
        TextView titleTV, userTV, bodyTV;
        public PostViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTV = itemView.findViewById(R.id.tv_id);
            userTV = itemView.findViewById(R.id.textView);

        }
    }
}
