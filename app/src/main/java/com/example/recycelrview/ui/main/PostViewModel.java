package com.example.recycelrview.ui.main;


import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.example.recycelrview.data.PostsClient;
import com.example.recycelrview.pojo.PostResponse;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostViewModel extends ViewModel
{
    MutableLiveData<PostResponse> postsMutableLiveData = new MutableLiveData<>();
    MutableLiveData<String> postsError = new MutableLiveData<>();

    //    TODO (step 4) update this Function.
    public void getPosts()
    {
        PostsClient.getINSTANCE().getPosts().enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response)
            {
                PostResponse body = response.body();
                // TODO (step 5) Adding "Log.d" function to track your code and see what is happening.
                Log.d("saif", "onResponse: code= "+response.code());
                Log.d("saif", "onResponse: body= "+new Gson().toJson(body));

                // TODO (step 7) Check if the response was successful Http Code(200->299).
                if (response.isSuccessful() && body != null  && body.getCod().equals("200"))
                    postsMutableLiveData.setValue(body);
                else
                    postsError.setValue("Something went Wrong");
            }
            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                postsError.setValue("Something went Wrong");
                // TODO (step 6) Adding "Log.e" function to track your code and see where is the error happened.


                Log.e("saif", "onFailure: error= ", t);
            }
        });
    }


}
