
package com.example.recycelrview.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/** TODO (step 2) Create this Model based on the response of API, Check the Documentation to understand the Attributes of API Response.
*         - you may use Postman Program (recommended) to see the response or just put the lik in the browser.
 *        - you also may use "json2Pojo" plugin to convert Json into Model in java.
*/

public class PostResponse {
    @SerializedName("city")
    private City mCity;
    @SerializedName("cnt")
    private Long mCnt;
    @SerializedName("cod")
    private String mCod;
    @SerializedName("list")
    private List<Post> mList;
    @SerializedName("message")
    private Double mMessage;

    public City getCity() {
        return mCity;
    }

    public void setCity(City city) {
        mCity = city;
    }

    public Long getCnt() {
        return mCnt;
    }

    public void setCnt(Long cnt) {
        mCnt = cnt;
    }

    public String getCod() {
        return mCod;
    }

    public void setCod(String cod) {
        mCod = cod;
    }

    public List<Post> getList() {
        return mList;
    }

    public void setList(List<Post> list) {
        mList = list;
    }

    public Double getMessage() {
        return mMessage;
    }

    public void setMessage(Double message) {
        mMessage = message;
    }

}
