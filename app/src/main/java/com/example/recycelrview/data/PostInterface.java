package com.example.recycelrview.data;

import com.example.recycelrview.pojo.PostResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PostInterface {

    // TODO (step 1) Remove "mode=xml" from the api name because the project support JSON only;
    @GET("/data/2.5/forecast?q=London,us&appid=439d4b804bc8187953eb36d2a8c26a02")
    Call<PostResponse> getPosts();
}
